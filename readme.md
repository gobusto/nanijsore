naniJS
======

naniJS is a collection of HTML + Javascript files aimed at learners who want
to self-test their knowledge of the Japanese language. The original version was
created in 2011, but it has since been re-written from scratch in order to make
the source code more readable and extensible.

All source code (and related files, such as images and auto-gen scripts) can be
downloaded from [GitLab](https://gitlab.com/gobusto/nanijsore/tree/master/). If
you can't (or don't want to) download anything, I usually have a recent version
available on my [website](http://kiniro.uk:8080).

Background
----------

A few years ago (probably sometime around 2007), I used to test my knowledge of
Japanese verb/adjective conjugation rules by selecting words at random, writing
down how I thought they should be conjugated on a piece of paper, and comparing
what I'd written with the examples from [about.com](http://www.about.com/):

+ [Verbs](http://japanese.about.com/library/weekly/aa031101b.htm)
+ [Adjectives](http://japanese.about.com/library/weekly/aa040101b.htm)

As you may imagine, this was slow, prone to error, and consumed a lot of paper.
I therefore created a basic Javascript page to automate the validation process,
eventually adding some extra conjugation types based on information provided by
Jim Breen's [WWWJDIC](http://nihongo.monash.edu/cgi-bin/wwwjdic).

I've since added separate pages for other aspects of the language, such a nouns
(when I realised that I didn't actually know many words, and needed to memorize
some) and kanji (as another way to force myself to memorize more than I already
knew).

It's important to note that **this _isn't_ intended to be a "Learn How To Speak
Japanese" website**; my language skills are nowhere near good enough to attempt
something like that, and there are already lots of places on the internet which
do a better job than I could possibly manage. Instead, my aim is to write a set
of useful "self-testing" pages which can be used as an aid to memorization (and
possibly as a way for me to improve my Javascript/CSS skills whilst I'm at it).

The one exception is the kana page, which was inspired by the sadly now-defunct
[KanjiSITE](http://web.archive.org/web/20040607152732/http://www.kanjisite.com/)
hiragana/katakana pages. I found the KanjiSITE to be really useful when I first
decided to learn about the Japanese language way back in 2003, so hopefully the
information on the kana page will be similarly useful to someone.

How It Works
------------

The various HTML/Javascript/CSS pages are designed to be self-contained, and do
not require third-party utilities such as jQuery or Bootstrap. It's possible to
use naniJS on a computer without an internet connection, which can be useful
if you're in an area with infrequent (or very slow) internet access.

The only "external" resource dependency is an *optional* line in `default.css`,
which attempts to load the Noto Sans font from Google Fonts, to ensure that CJK
characters can be displayed correctly; this isn't mandatory and can be removed.

Pages should work correctly on just about any modern browser. Personally, I use
Firefox and Chrome on my development machine and Chrome for Android on tablets.
Phone-sized devices *should* work correctly but aren't officially supported due
to their rather small screen size (and because entering text is rather fiddly).

In addition to the main HTML/Javascript/CSS files, a couple of "helper" scripts
are used to auto-generate various things:

+ `update_layout.py` replaces the boilerplate HTML in all pages with the layout
  defined by `_layout.html`. It has been tested with Python 2.7 and Python 3.4.
+ `update_everything.sh` converts the contents of `readme.md` into HTML (tested
  using discount 2.1) and then runs `update_layout.py` to add the "layout" HTML
  to it (along with every other HTML file).

There are also a few test cases for the Javascript files in `js/util/tests`. As
with the helper scripts, these are "support" files, so aren't necessary for the
pages themselves to function.

TODO
----

The current version of the codebase is very much a work in progress; issues not
yet addressed are outlined below:

+ **nouns.html** - Add more words/categories (and support more languages).
+ **particles.html** - Add more example sentences and randomise the selection.
+ **conjugate.html** - Need to add more conjugations (based on WWWJDIC).
+ **help.html** - Need to improve the CSS for the "conversion process" stuff.

It would also be nice to include the following bits somewhere:

+ Simultaneous actions, using the ながら verb ending. For details, see the page on
[WikiBooks](http://en.wikibooks.org/wiki/Japanese/Lesson/Simultaneous_action).
+ Expressing "want" via 欲しい or the たい verb ending. For details, see the page on
[about.com](http://japanese.about.com/od/Grammar/a/Expressions-Of-Desire.htm) or
[this Stack Exchange answer](https://japanese.stackexchange.com/a/52621/9212).

...plus LibreJS stuff: <https://www.gnu.org/licenses/javascript-labels.html>

<!--

Running jshint and jsdoc
------------------------

First, install NodeJS and NPM:

    sudo apt install npm
    sudo apt install nodejs-legacy

Next, install the jsdoc and jshint packages:

    npm install jsdoc
    npm install jshint

To run either of these, use the following commands:

    $(npm bin)/jsdoc ./js
    $(npm bin)/jshint ./js

Note that neither of these are necessary if you simply want to run naniJS.

-->

License
-------

Copyright (c) 2015-2017 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Contact
-------

I originally made this for myself as an automated self-testing excercise, so it
is possible that I've made some silly mistakes. If you come across any, send me
an email: `goFULLSTOPbustoATgmailFULLSTOPcom`
