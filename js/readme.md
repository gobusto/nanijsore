General Javascript
==================

The files in this directory store the "main" JS code for the corresponding HTML
page in the top-level directory. They handle the actual DOM manipulation, etc.
