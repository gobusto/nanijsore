/**
@file Code specific to the nouns.html page.
*/

(function () {
  "use strict";

  if (!window.DICTIONARY) window.DICTIONARY = {};

  window.DICTIONARY.de = {
    title: "Nomen",
    intro: "???",
    buttons: "???",

    name: "Deutsch",
    dictionary: {
      "fruit": ["obst"]
    }
  };
})();
