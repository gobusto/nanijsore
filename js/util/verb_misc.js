/**
@file Utilities for conjugating irregular Japanese verbs.
*/

(function () {
  "use strict";
  /* jshint -W086 */

  if (!window.naniJS) window.naniJS = {};

  window.naniJS.VERB_MISC = {
    about: "Irregular verb",
    tense_list: [
      "present",
      "past",
      "te",
      //"conditional",
      "provisional",
      "potential",
      "passive",
      "causative",
      "volitional",
      //"conjectural",
      //"alternative",
      "imperative"
    ],

    word_list: [
      { kanji: "為る", kana: "する", English: "to do", prefer_kana: true },
      { kanji: "来る", kana: "くる", English: "to come" }
    ],

    imperative: function(word, options) {
      if (!options) options = {};

      var stem = word[0];
      if (stem == "す") stem = "し";
      if (stem == "く") stem = options.formal ? "き" : "こ";

      if (options.formal)
        return options.negative ? [stem + "なさるな"] : [stem + "なさい"];
      else if (options.negative)
        return [word + "な"];
      else
        return (stem == '来' || stem == 'こ') ? [stem + "い"] : [stem + "ろ"];
    },

    conjectural: function(word, options) {
      if (!options) options = {};
      word = this.present(word, { negative: options.negative })[0];
      return [word + (options.formal ? 'でしょう' : 'だろう')];
    },

    volitional: function(word, options) {
      if (!options) options = {};

      var stem = word[0];
      if (stem == "す") stem = "し";
      if (stem == "く") stem = options.formal ? "き" : "こ";

      if (options.formal)
        return options.negative ? [stem + "ますまい"] : [stem + "ましょう"];
      else
        return options.negative ? [word + "まい"] : [stem + "よう"];
    },

    causative: function(word, options) {
      if (!options) options = {};

      var stem = word[0];
      if (stem == 'す') stem = 'さ';
      if (stem == 'く') stem = 'こさ';
      if (stem == '来') stem = '来さ';

      if (options.formal) {
        if (options.negative)
          return [stem + 'せません', stem + 'しません'];
        else
          return [stem + 'せます', stem + 'します'];
      } else {
        if (options.negative)
          return [stem + 'せない', stem + 'さない'];
        else
          return [stem + 'せる', stem + 'す'];
      }
    },

    passive: function(word, options) {
      if (!options) options = {};

      var stem = (word[0] == 'す' ? 'さ' : '為') + 'れ';

      if (word == "為る" || word == "する") {
        if (options.formal)
          return options.negative ? [stem + 'ません'] : [stem + 'ます'];
        else
          return options.negative ? [stem + 'ない'] : [stem + 'る'];
      }

      return this.potential(word, options);
    },

    potential: function(word, options) {
      if (!options) options = {};

      if (word == "為る" || word == "する") {
        if (options.formal)
          return options.negative ? ["できません"] : ["できます"];
        else
          return options.negative ? ["できない"] : ["できる"];
      }

      if (word == "来る" || word == "くる") {
        var prefix = (word == '来る' ? '来' : 'こ') + 'られ';
        if (options.formal)
          return options.negative ? [prefix + 'ません'] : [prefix + 'ます'];
        else
          return options.negative ? [prefix + 'ない'] : [prefix + 'る'];
      }
    },

    provisional: function(word, options) {
      if (!options) options = {};
      word = this.present(word, options)[0];

      if (options.formal)
        return [word + "なら", word + "ならば"];
      else if (options.negative)
        return [word.slice(0, -1) + "ければ"];
      else
        return [word.slice(0, -1) + "れば"];
    },

    te: function(word, options) {
      if (!options) options = {};

      if (options.negative)
        word = this.present(word, options)[0] + "で";
      else
        word = this.past(word, options)[0].slice(0, -1) + "て";

      if (options.negative && !options.formal)
        return [word, word.slice(0, -2) + "くて"];
      else
        return [word];
    },

    past: function(word, options) {
      if (!options) options = {};

      var stem = word[0];
      if (stem == "す") stem = "し";
      if (stem == "く") stem = options.negative && !options.formal ? "こ" : "き";

      if (options.formal)
        return options.negative ? [stem + "ませんでした"] : [stem + "ました"];
      else
        return options.negative ? [stem + "なかった"] : [stem + "た"];
    },

    present: function(word, options) {
      if (!options) options = {};

      var stem = word[0];
      if (stem == "す") stem = "し";
      if (stem == "く") stem = options.formal ? "き" : "こ";

      if (options.formal)
        return options.negative ? [stem + "ません"] : [stem + "ます"];
      else
        return options.negative ? [stem + "ない"] : [word];
    },

    conjugate: function(word, tense, options) {
      switch (tense) {
        case "imperative":  return this.imperative(word, options);
        case "alternative": return [this.past(word, options)[0] + "り"];
        case "conjectural": return this.conjectural(word, options);
        case "volitional":  return this.volitional(word, options);
        case "causative":   return this.causative(word, options);
        case "passive":     return this.passive(word, options);
        case "potential":   return this.potential(word, options);
        case "provisional": return this.provisional(word, options);
        case "conditional": return [this.past(word, options)[0] + "ら"];
        case "te":          return this.te(word, options);
        case "past":        return this.past(word, options);
        default:            return this.present(word, options);
      }
    }
  };
})();
