Support JS
==========

The files in this directory contain "support" stuff (extra string methods, data
sets for nouns/kanji/etc.) and are largely independent from everything else, so
it should be possible to re-use them in other projects without too much hassle.
