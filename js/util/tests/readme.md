Tests
=====

These files run a few tests on the various bits of Javascript used by the pages
for interactive stuff. They're only really here to make sure that I don't break
anything when I make changes - they aren't necessary to make the pages work.
