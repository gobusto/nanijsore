/**
@file Contains the code used to actually run the tests.
*/

(function () {
  "use strict";

  function main() {
    var result = [];

    TEST_PACK.forEach(function (test) {
      result.push(test.runTest());
      document.body.innerHTML = ""; // Clear any elements created by the test.
    });

    document.body.appendChild(document.createElement('h1')).innerHTML = 'Results';

    TEST_PACK.forEach(function (test, i) {
      var t = ['Test #', String(i+1), ' ', test.description, ': '].join('');
      if (result[i] === test.expected)
        t += 'Pass';
      else
        t += ['Fail; Expected "', test.expected, '", got "', result[i], '"'].join('');
      document.body.appendChild(document.createElement('div')).innerHTML = t;
    });
  }

  window.onload = main;
})();
