/**
@file Tests for the katakana/hiragana/romaji string methods added by utils.js
*/

(function () {
  "use strict";

  if (!window.TEST_PACK) window.TEST_PACK = [];

  window.TEST_PACK.push({
    description: 'Katakana to hiragana',
    expected: 'ふぁっしょん',
    runTest: function() { return 'ファッション'.toHiragana(); }
  });

  window.TEST_PACK.push({
    description: 'Hiragana to katakana',
    expected: 'ファッション',
    runTest: function() { return 'ふぁっしょん'.toKatakana(); }
  });

  window.TEST_PACK.push({
    description: 'Hiragana to hiragana',
    expected: 'ふぁっしょん',
    runTest: function() { return this.expected.toHiragana(); }
  });

  window.TEST_PACK.push({
    description: 'Katakana to katakana',
    expected: 'ファッション',
    runTest: function() { return this.expected.toKatakana(); }
  });

  window.TEST_PACK.push({
    description: 'Romaji to hiragana',
    expected: 'ふぁっしょおんにゃーXりゅろううおおんでっのつうん',
    runTest: function() { return "fasshoon'nya-Xryurouuoondennotsuun".toKana(); }
  });

  window.TEST_PACK.push({
    description: 'Katakana to romaji',
    expected: "FASSHOON'NYA-xRUROUUOON'DENNOTSUUN'",
    runTest: function() { return 'ファッショオンニャーxルロウウオオンデッノツウン'.toRomaji(); }
  });
})();
