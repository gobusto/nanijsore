/**
@file Code specific to the nouns.html page.
*/

(function () {
  "use strict";

  if (!window.DICTIONARY) window.DICTIONARY = {};

  window.DICTIONARY.jp = {
    title: "名詞",
    intro: "???",
    buttons: "???",

    name: "日本語",
    dictionary: {
      "fruit": ["実", "み", "ミ", "mi"],

      "bird": ["鳥", "とり", "トリ", "tori"],
      "cat": ["猫", "ねこ", "ネコ", "neko"],
      "cow": ["牛", "うし", "ウシ", "ushi"],
      "dog": ["犬", "いぬ", "イヌ", "inu"],
      "elephant": ["象", "ぞう", "ゾウ", "zou"],
      "horse": ["馬", "うま", "ウマ", "uma"],
      "mouse": ["鼠", "ねずみ", "ネズミ", "nezumi"],
      "octopus": ["蛸", "たこ", "タコ", "tako"],  // Are 章魚 and 鮹 also OK?
      "pig": ["豚", "ぶた", "ブタ", "buta"],
      "rabbit": ["兎", "うさぎ", "ウサギ", "usagi"],
      "tiger": ["虎", "とら", "トラ", "tora"],

      "ear": ["耳", "みみ", "ミミ", "mimi"],
      "eye": ["目", "め", "メ", "me"],
      "finger": ["指", "ゆび", "ユビ", "yubi"],
      "foot": ["足", "あし", "アシ", "ashi"],
      "hair": ["髪", "かみ", "カミ", "kami"],
      "hand": ["手", "て", "テ", "te"],
      "mouth": ["口", "くち", "クチ", "kuchi"],  // Perhaps also allow 唇／くちびる (lips)?
      "nose": ["鼻", "はな", "ハナ", "hana"],

      "apple": ["林檎", "りんご", "リンゴ", "ringo"],
      "banana": ["甘蕉", "ばなな", "バナナ", "banana"],
      "tangerine": ["蜜柑", "みかん", "ミカン", "mikan"],
      "peach": ["桃", "もも", "モモ", "momo"],
      "pear": ["梨", "なし", "ナシ", "nashi"],
      "pineapple": ["鳳梨", "ほうり", "ホウリ", "houri", "パイナップル", "ぱいなっぷる", "painappuru"],
      "strawberry": ["苺", "いちご", "イチゴ", "ichigo"],
      "watermelon": ["西瓜", "すいか", "スイカ", "suika"],

      "monday": ["月曜日", "げつようび", "ゲツヨウビ", "getsuyoubi"],
      "tuesday": ["火曜日", "かようび", "カヨウビ", "kayoubi"],
      "wednesday": ["水曜日", "すいようび", "スイヨウビ", "suiyoubi"],
      "thursday": ["木曜日", "もくようび", "モクヨウビ", "mokuyoubi"],
      "friday": ["金曜日", "きんようび", "キンヨウビ", "kinyoubi"],
      "saturday": ["土曜日", "どようび", "ドヨウビ", "doyoubi"],
      "sunday": ["日曜日", "にちようび", "ニチヨウビ", "nichiyoubi"],

      "book": ["本", "ほん", "ホン", "hon"],
      "comic": ["漫画", "まんが", "マンガ", "manga"],
      "letter": ["手紙", "てがみ", "テガミ", "tegami"],
      "magazine": ["雑誌", "ざっし", "ザッシ", "zasshi"],
      "newspaper": ["新聞", "しんぶん", "シンブン", "shinbun"]
    }
  };
})();
