/**
@file Utilities for conjugating -na adjectives.
*/

(function () {
  "use strict";

  if (!window.naniJS) window.naniJS = {};

  window.naniJS.ADJECTIVE_NA = {
    about: "~な adjective",
    tense_list: ["present", "past"],

    word_list: [
      { kanji: "元気な", kana: "げんきな", English: "healthy" },
      { kanji: "有名な", kana: "ゆうめいな", English: "famous" },
      { kanji: "派手な", kana: "はでな", English: "showy" }
    ],

    conjugate: function(word, tense, options) {
      word = word.slice(0, -1);  // Remove the な from the end.
      if (options.formal)
        if (options.negative)
          if (tense == "past")
            return [word + "じゃありませんでした", word + "ではありませんでした"];
          else
            return [word + "じゃありません", word + "ではありません"];
        else
          if (tense == "past")
            return [word + "でした"];
          else
            return [word + "です"];
      else
        if (options.negative)
          if (tense == "past")
            return [word + "じゃなかった", word + "ではなかった"];
          else
            return [word + "じゃない", word + "ではない"];
        else
          if (tense == "past")
            return [word + "だった"];
          else
            return [word + "だ"];
    }
  };
})();
