/**
@file Utilities for conjugating -u verbs.
*/

(function () {
  "use strict";

  if (!window.naniJS) window.naniJS = {};

  window.naniJS.SENTENCES = {
    は: [
      {
        jp: "私_日本人です。",
        en: 'I am Japanese.',
        incorrect: {
          particles: ['で', 'より']
        }
      }
    ],
    の: [
      {
        jp: "これは私_本です。",
        en: 'This is my book.',
        incorrect: {
          particles: ['で', 'しか']
        }
      }
    ],
    も: [
      {
        jp: "その車_田中さんのです。",
        en: 'That car also belongs to Mr. Tanaka.',
        incorrect: {
          particles: ['で', 'は']
        }
      }
    ],
    で: [
      {
        jp: "図書館は英語_何ですか。",
        en: 'What is "library" in English?',
        incorrect: {
          particles: ['の', 'より']
        }
      }
    ],
    しか: [
      {
        jp: 'ＧＩＦ－ｆｉｌｅｓ_読めません。',
        en: 'I can only import GIF-files.',
        incorrect: {
          particles: ['で', 'かしら']
        }
      },
      {
        jp: '２０人の生徒のうちたった一人_その本を読んだことがなかった。',
        en: 'Out of the students, only one had read that book.',
        incorrect: {
          particles: ['から', 'けど']
        }
      }
    ],
    より: [
      {
        jp: '町は村_も大きい。',
        en: 'Towns are larger than villages.',
        incorrect: {
          particles: ['は', 'かい']
        }
      },
      {
        jp: '彼女は前_元気そうだった。',
        en: 'She looked better than last time.',
        incorrect: {
          particles: ['しか', 'ばかり']
        }
      }
    ]
  };
})();
