/**
@file Utilities for conjugating -u verbs.
*/

(function () {
  "use strict";
  /* jshint -W086 */

  if (!window.naniJS) window.naniJS = {};

  window.naniJS.VERB_U = {
    about: "五段 verb",
    tense_list: [
      "present",
      "past",
      "te",
      //"conditional",
      "provisional",
      "potential",
      "passive",
      "causative",
      "volitional",
      //"conjectural",
      //"alternative",
      "imperative"
    ],

    word_list: [
      { kanji: "写す", kana: "うつす", English: "to copy" },
      { kanji: "書く", kana: "かく", English: "to write" },
      { kanji: "聞く", kana: "きく", English: "to listen" },
      { kanji: "急ぐ", kana: "いそぐ", English: "to hurry" },
      { kanji: "会う", kana: "あう", English: "to meet" },
      { kanji: "待つ", kana: "まつ", English: "to wait" },
      { kanji: "帰る", kana: "かえる", English: "to leave, to go back" },
      { kanji: "死ぬ", kana: "しぬ", English: "to die" },
      { kanji: "遊ぶ", kana: "あそぶ", English: "to play" },
      { kanji: "飲む", kana: "のむ", English: "to drink" },
      { kanji: "読む", kana: "よむ", English: "to read" },
      { kanji: "話す", kana: "はなす", English: "to speak" }
    ],

    imperative: function(word, options) {
      if (!options) options = {};

      if (options.formal)
        return options.negative ? [this._i(word) + "なさるな"] : [this._i(word) + "なさい"];
      else
        return options.negative ? [word + "な"] : [this._e(word)];
    },

    volitional: function(word, options) {
      if (!options) options = {};
      word = this.present(word, { formal: options.formal })[0];

      if (options.negative)
        return [word + "まい"]; // 飲む --> 飲む(まい) / 飲みます --> 飲みます(まい)
      else if (options.formal)
        return [word.slice(0, -1) + "しょう"]; // 飲みま(す) --> 飲みま(しょう)
      else
        return [this._o(word) + "う"]; // 飲(む) --> 飲(もう)
    },

    conjectural: function(word, options) {
      if (!options) options = {};
      word = this.present(word, { negative: options.negative })[0];
      return [word + (options.formal ? 'でしょう' : 'だろう')];
    },

    causative: function(word, options) {
      if (!options) options = {};
      var stem = this._a(word);

      if (options.formal) {
        if (options.negative)
          return [stem + 'せません', stem + 'しません'];
        else
          return [stem + 'せます', stem + 'します'];
      } else {
        if (options.negative)
          return [stem + 'せない', stem + 'さない'];
        else
          return [stem + 'せる', stem + 'す'];
      }
    },

    potential: function(word, options) {
      if (!options) options = {};
      word = this._e(word);

      if (options.formal)
        return options.negative ? [word + "ません"] : [word + "ます"];
      else
        return options.negative ? [word + "ない"] : [word + "る"];
    },

    provisional: function(word, options) {
      if (!options) options = {};
      word = this.present(word, options)[0];

      if (options.formal)
        return [word + "なら", word + "ならば"];
      else if (options.negative)
        return [word.slice(0, -1) + "ければ"];
      else
        return [this._e(word) + "ば"];
    },

    te: function(word, options) {
      if (!options) options = {};

      if (options.negative)
        word = this.present(word, options)[0] + "で";
      else {
        word = this.past(word, options)[0];
        if (word.slice(-1) == "だ")
          word = word.slice(0, -1) + "で";
        else
          word = word.slice(0, -1) + "て";
      }

      if (options.negative && !options.formal)
        return [word, word.slice(0, -2) + "くて"];
      else
        return [word];
    },

    past: function(word, options) {
      if (!options) options = {};

      if (options.formal)
        return options.negative ? [this._i(word) + "ませんでした"] : [this._i(word) + "ました"];
      else　if (options.negative)
        return [this._a(word) + "なかった"];
      else switch (word.slice(-1)) {
        case 'く':
          return [word.slice(0, -1) + "いた"];
        case 'ぐ':
          return [word.slice(0, -1) + "いだ"];
        case 'う': case 'つ': case 'る':
          return [word.slice(0, -1) + "った"];
        case 'ぬ': case 'ぶ': case 'む':
          return [word.slice(0, -1) + "んだ"];
        case 'す':
          return [word.slice(0, -1) + "した"];
      }
    },

    present: function(word, options) {
      if (!options) options = {};

      if (options.formal)
        return options.negative ? [this._i(word) + "ません"] : [this._i(word) + "ます"];
      else
        return options.negative ? [this._a(word) + "ない"] : [word];
    },

    _a: function(word) {
      var ending = {
        く:"か", ぐ:"が", う:"わ", つ:"た", る:"ら", ぬ:"な", ぶ:"ば", む:"ま", す:"さ"
      };
      return word.slice(0, -1) + ending[word.slice(-1)];
    },

    _i: function(word) {
      var ending = {
        く:"き", ぐ:"ぎ", う:"い", つ:"ち", る:"り", ぬ:"に", ぶ:"び", む:"み", す:"し"
      };
      return word.slice(0, -1) + ending[word.slice(-1)];
    },

    _e: function(word) {
      var ending = {
        く:"け", ぐ:"げ", う:"え", つ:"て", る:"れ", ぬ:"ね", ぶ:"べ", む:"め", す:"せ"
      };
      return word.slice(0, -1) + ending[word.slice(-1)];
    },

    _o: function(word) {
      var ending = {
        く:"こ", ぐ:"ご", う:"お", つ:"と", る:"ろ", ぬ:"の", ぶ:"ぼ", む:"も", す:"そ"
      };
      return word.slice(0, -1) + ending[word.slice(-1)];
    },

    conjugate: function(word, tense, options) {
      switch (tense) {
        case "imperative":  return this.imperative(word, options);
        case "alternative": return [this.past(word, options)[0] + "り"];
        case "conjectural": return this.conjectural(word, options);
        case "volitional":  return this.volitional(word, options);
        case "causative":   return this.causative(word, options);
        case "passive":     return this.potential(this._a(word) + 'る', options);
        case "potential":   return this.potential(word, options);
        case "provisional": return this.provisional(word, options);
        case "conditional": return [this.past(word, options)[0] + "ら"];
        case "te":          return this.te(word, options);
        case "past":        return this.past(word, options);
        default:            return this.present(word, options);
      }
    }
  };
})();
