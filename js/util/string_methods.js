/**
@file Defines some extra string methods for handling hiragana and katakana.

@todo Would be nice to have a half-to-full-width katakana conversion function.

@todo Would also be nice to have a regular-to-wide latin conversion function.
*/

(function () {
  "use strict";
  /* jshint validthis:true */

  /**
  @brief Helper function: Replace A with B *or* B with A.

  @param text The orignal text.
  @param a The first string.
  @param b The second string.
  @param reverse If true, replace b with a, otherwise replace a with b.
  @return A new string, with all instances of A/B replaced with B/A.
  */

  function gsub(text, a, b, reverse) {
    return text.replace(new RegExp(reverse ? b : a, 'g'), reverse ? a : b);
  }

  /**
  @brief Replace katakana characters with their hiragana equivalents.

  The following characters are not handled, as they don't seem to have hiragana
  equivalents:

  + ヷ (VA)
  + ヸ (VI)
  + ヹ (VE)
  + ヺ (VO)

  @param backwards If true, convert HIRAGANA into KATAKANA instead.
  @return A new string, with katakana characters replaced by hiragana.
  */

  function toHiragana(backwards) {
    var len = this.length;
    var t = '';
    for (var i = 0; i < len; ++i) {
      var c = this.charCodeAt(i);
      if      (!backwards && c >= 0x30A1 && c <= 0x30F6) c -= 96;
      else if ( backwards && c >= 0x3041 && c <= 0x3096) c += 96;
      t += String.fromCharCode(c);
    }
    return t;
  }

  /**
  @brief Replace hiragana characters with their katakana equivalents.

  @return A new string, with hiragana characters replaced by katakana.
  */

  function toKatakana() { return toHiragana.bind(this)(true); }

  /**
  @brief Convert hiragana to lower-case latin and katakana to upper-case latin.

  The conversion process isn't particulary clever; No attempt is made to detect
  cases where は or へ are being used as particles, and Kanji are simply ignored.

  @param backwards If true, convert ROMAJI into KANA instead.
  @return A new string in which kana characters have been replaced by latin text.
  */

  function toRomaji(backwards) {

    var convert = function(t, a, b) {
      var A = toKatakana.call(a);
      var B = b.toUpperCase();
      if (b.length > 1)
        t = gsub(gsub(t, 'っ' + a, b.slice(0, 1) + b, backwards),
                         'ッ' + A, B.slice(0, 1) + B, backwards);
      return gsub(gsub(t, a, b, backwards), A, B, backwards);
    };

    var SMALL_Y = {'ya':'ゃ','yu':'ゅ','yo':'ょ'};
    var SMALL_V = {'a':'ぁ','i':'ぃ','e':'ぇ','o':'ぉ'};
    var ROMAJI_BY_LENGTH = [{'shi':'し','chi':'ち','tsu':'つ'},{
      'ka':'か','ki' :'き','ku':'く','ke':'け','ko':'こ',
      'ga':'が','gi' :'ぎ','gu':'ぐ','ge':'げ','go':'ご',
      'sa':'さ',           'su':'す','se':'せ','so':'そ',
      'za':'ざ','ji' :'じ','zu':'ず','ze':'ぜ','zo':'ぞ',
      'ta':'た',                     'te':'て','to':'と',
      'da':'だ','di' :'ぢ','du':'づ','de':'で','do':'ど',
      'na':'な','ni' :'に','nu':'ぬ','ne':'ね','no':'の',
      'ha':'は','hi' :'ひ','fu':'ふ','he':'へ','ho':'ほ',
      'ba':'ば','bi' :'び','bu':'ぶ','be':'べ','bo':'ぼ',
      'pa':'ぱ','pi' :'ぴ','pu':'ぷ','pe':'ぺ','po':'ぽ',
      'ma':'ま','mi' :'み','mu':'む','me':'め','mo':'も',
      'ra':'ら','ri' :'り','ru':'る','re':'れ','ro':'ろ',
      'wa':'わ','wi' :'ゐ','vu':'ゔ','we':'ゑ','wo':'を',
      'ya':'や',           'yu':'ゆ',          'yo':'よ'
    },{'a':'あ','i':'い','u':'う','e':'え','o' :'お',"n'" :'ん'}];

    var text = this;
    for (var i = 0; i < ROMAJI_BY_LENGTH.length; ++i)
      for (var romaji in ROMAJI_BY_LENGTH[i]) {
        var kana = ROMAJI_BY_LENGTH[i][romaji];
        var start = romaji.slice(0, -1);  // 'shi' -> 'sh'
        var vowel = romaji.slice(   -1);  // 'shi' -> 'i'

        if (romaji != 'i' && vowel == 'i')
          for (var y in SMALL_Y)
            if (['sh','ch','j'].indexOf(start) >= 0)
              text = convert(text, kana+SMALL_Y[y], start+y.slice(-1));
            else
              text = convert(text, kana+SMALL_Y[y], start+y);
        else if (romaji == 'fu' || romaji == 'vu')
          for (var v in SMALL_V)
            text = convert(text, kana+SMALL_V[v], start+v);
        text = convert(text, kana, romaji);
      }
    return gsub(gsub(text, 'ー', '-', backwards), 'ん', 'n', backwards);
  }

  /**
  @brief Convert latin text into kana format.

  @return A new string in which latin characters have been replaced by kana text.
  */

  function toKana() { return toRomaji.bind(this)(true); }

  /*
  Comment these lines out if you don't want to pollute the String prototype with
  extra methods; you can still call the functions using `toKatakana.call("あ")`
  */

  String.prototype.toHiragana = toHiragana;
  String.prototype.toKatakana = toKatakana;
  String.prototype.toRomaji = toRomaji;
  String.prototype.toKana = toKana;
})();
