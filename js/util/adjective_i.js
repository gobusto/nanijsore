/**
@file Utilities for conjugating -i adjectives.
*/

(function () {
  "use strict";

  if (!window.naniJS) window.naniJS = {};

  window.naniJS.ADJECTIVE_I = {
    about: "~い adjective",
    tense_list: ["present", "past"],

    word_list: [
      { kanji: "広い", kana: "ひろい", English: "wide" },
      { kanji: "太い", kana: "ふとい", English: "thick" },
      { kanji: "古い", kana: "ふるい", English: "old" },
      { kanji: "長い", kana: "ながい", English: "long" },
      { kanji: "高い", kana: "たかい", English: "high/expensive" }
    ],

    conjugate: function(word, tense, options) {
      word = word.slice(0, -1);  // Remove the い from the end.
      if (options.formal)
        if (options.negative)
          if (tense == "past")
            return [word + "くなかったです", word + "くありませんでした"];
          else
            return [word + "くないです", word + "くありません"];
        else
          if (tense == "past")
            return [word + "かったです"];
          else
            return [word + "いです"];
      else
        if (options.negative)
          if (tense == "past")
            return [word + "くなかった"];
          else
            return [word + "くない"];
        else
          if (tense == "past")
            return [word + "かった"];
          else
            return [word + "い"];
    }
  };
})();
