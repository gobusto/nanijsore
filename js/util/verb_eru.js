/**
@file Utilities for conjugating -iru/eru verbs.
*/

(function () {
  "use strict";
  /* jshint -W086 */

  if (!window.naniJS) window.naniJS = {};

  window.naniJS.VERB_ERU = {
    about: "一段 verb",
    tense_list: [
      "present",
      "past",
      "te",
      //"conditional",
      "provisional",
      "potential",
      "passive",
      "causative",
      "volitional",
      //"conjectural",
      //"alternative",
      "imperative"
    ],

    word_list: [
      { kanji: "見る", kana: "みる", English: "to see" },
      { kanji: "寝る", kana: "ねる", English: "to sleep" },
      { kanji: "走る", kana: "はしる", English: "to run" },
      { kanji: "食べる", kana: "たべる", English: "to eat" },
      { kanji: "考える", kana: "かんがえる", English: "to think about" }
    ],

    imperative: function(word, options) {
      if (!options) options = {};
      var stem = word.slice(0, -1);  // 食べる --> 食べ

      if (options.formal) {
        return options.negative ? [stem + 'なさるな'] : [stem + 'なさい'];
      } else {
        return options.negative ? [stem + 'るな'] : [stem + 'ろ'];
      }
    },

    conjectural: function(word, options) {
      if (!options) options = {};
      word = this.present(word, { negative: options.negative })[0];
      return [word + (options.formal ? 'でしょう' : 'だろう')];
    },

    volitional: function(word, options) {
      if (!options) options = {};
      var stem = word.slice(0, -1);  // 食べる --> 食べ

      if (options.formal) {
        return options.negative ? [stem + 'ますまい'] : [stem + 'ましょう'];
      } else {
        return options.negative ? [stem + 'まい'] : [stem + 'よう'];
      }
    },

    causative: function(word, options) {
      if (!options) options = {};
      var stem = word.slice(0, -1) + 'さ';

      if (options.formal) {
        if (options.negative)
          return [stem + 'せません', stem + 'しません'];
        else
          return [stem + 'せます', stem + 'します'];
      } else {
        if (options.negative)
          return [stem + 'せない', stem + 'さない'];
        else
          return [stem + 'せる', stem + 'す'];
      }
    },

    potential: function(word, options) {
      if (!options) options = {};
      var stem = word.slice(0, -1);  // 食べる --> 食べ

      if (options.formal)
        return options.negative ? [stem + "られません"] : [stem + "られます"];
      else
        return options.negative ? [stem + "られない"] : [stem + "られる"];
    },

    provisional: function(word, options) {
      if (!options) options = {};
      var stem = word.slice(0, -1);  // 食べる --> 食べ

      if (!options.formal)
        return options.negative ? [stem + "なければ"] : [stem + "れば"];
      else if (options.negative)
        return [stem + "ませんなら", stem + "ませんならば"];
      else
        return [stem + "ますなら", stem + "ますならば"];
    },

    te: function(word, options) {
      if (!options) options = {};

      if (options.negative)
        word = this.present(word, options)[0] + "で";
      else
        word = this.past(word, options)[0].slice(0, -1) + "て";

      if (options.negative && !options.formal)
        return [word, word.slice(0, -2) + "くて"];
      else
        return [word];
    },

    past: function(word, options) {
      if (!options) options = {};
      var stem = word.slice(0, -1);  // 食べる --> 食べ

      if (options.formal)
        return options.negative ? [stem + "ませんでした"] : [stem + "ました"];
      else
        return options.negative ? [stem + "なかった"] : [stem + "た"];
    },

    present: function(word, options) {
      if (!options) options = {};
      var stem = word.slice(0, -1);  // 食べる --> 食べ

      if (options.formal)
        return options.negative ? [stem + "ません"] : [stem + "ます"];
      else
        return options.negative ? [stem + "ない"] : [stem + "る"];
    },

    conjugate: function(word, tense, options) {
      switch (tense) {
        case "imperative":  return this.imperative(word, options);
        case "alternative": return [this.past(word, options)[0] + "り"];
        case "conjectural": return this.conjectural(word, options);
        case "volitional":  return this.volitional(word, options);
        case "causative":   return this.causative(word, options);
        case "passive":     return this.potential(word, options);
        case "potential":   return this.potential(word, options);
        case "provisional": return this.provisional(word, options);
        case "conditional": return [this.past(word, options)[0] + "ら"];
        case "te":          return this.te(word, options);
        case "past":        return this.past(word, options);
        default:            return this.present(word, options);
      }
    }
  };
})();
