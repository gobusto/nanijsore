/**
@file Code specific to the nouns.html page.
*/

(function () {
  "use strict";

  if (!window.DICTIONARY) window.DICTIONARY = {};

  window.DICTIONARY.en = {
    title: "Nouns",
    intro: "Enter an answer next to each item below, and then click the relevant button at the bottom of the page",
    buttons: "Which language did you use to answer the questions above?",

    name: "English",
    dictionary: {
      "animal": ["animal"],
      "bodypart": ["bodypart"],
      "color": ["color", "colour"],
      "fruit": ["fruit"],
      "weekdays": ["weekdays"],
      "writing": ["writing"],

      "bird": ["bird"],
      "cat": ["cat"],
      "cow": ["cow"],
      "dog": ["dog"],
      "elephant": ["elephant"],
      "horse": ["horse"],
      "mouse": ["mouse"],
      "octopus": ["octopus"],
      "pig": ["pig"],
      "rabbit": ["rabbit", "bunny"],
      "tiger": ["tiger"],

      "ear": ["ear"],
      "eye": ["eye"],
      "finger": ["finger"],  // Perhaps also allow "digit"?
      "foot": ["foot"],
      "hair": ["hair"],
      "hand": ["hand"],
      "mouth": ["mouth"],  // Perhaps also allow lips? Or use a better picture...
      "nose": ["nose"],

      "red": ["red"],
      "orange": ["orange"],
      "yellow": ["yellow"], // Not sure if Gold is OK here.
      "green": ["green"],
      "teal": ["teal", "aqua", "cyan", "turquoise"],
      "blue": ["blue"], // Not sure if Cerulean, Navy or Ultramarine are OK here.
      "purple": ["purple"],  // Not sure if Magenta, Violet or Indigo are OK here.
      "pink": ["pink"],
      "brown": ["brown"],  // Not sure if Tan is OK here.
      "black": ["black"],
      "grey": ["grey", "gray", "silver"],
      "white": ["white"],

      "apple": ["apple"],
      "banana": ["banana"],
      "tangerine": ["orange", "tangerine", "satsuma"],
      "peach": ["peach", "nectarine"],
      "pear": ["pear"],
      "pineapple": ["pineapple"],
      "strawberry": ["strawberry"],
      "watermelon": ["watermelon"],

      "monday": ["monday"],
      "tuesday": ["tuesday"],
      "wednesday": ["wednesday"],
      "thursday": ["thursday"],
      "friday": ["friday"],
      "saturday": ["saturday"],
      "sunday": ["sunday"],

      "book": ["book", "novel"],
      "comic": ["comic"],
      "letter": ["letter"],
      "magazine": ["magazine"],
      "newspaper": ["newspaper"]
    }
  };
})();
