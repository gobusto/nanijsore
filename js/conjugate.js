/**
@file Code specific to the conjugate.html page.
*/

(function () {
  "use strict";

  /**
  @brief This stores the various question types (stored in other files).
  */

  var QUESTION = [
    window.naniJS.ADJECTIVE_I,
    window.naniJS.ADJECTIVE_NA,
    window.naniJS.VERB_U,
    window.naniJS.VERB_ERU,
    window.naniJS.VERB_MISC
  ];

  /**
  @brief This function is called when the page first loads.
  */

  function setup() {
    // Set up the buttons:
    document.querySelector("#js-check").onclick = check;
    document.querySelector("#js-reset").onclick = reset;

    // Generate some HTML elements for each question.
    var parent = document.getElementById("quizarea");
    QUESTION.forEach(function (dict, i) {
      // Create a <div> to contain the various sub-elements.
      var root = parent.appendChild(document.createElement("div"));
      root.id = "question" + String(i);
      // This is the question title, such as "~u verbs".
      var title = root.appendChild(document.createElement('h2'));
      title.setAttribute("class", "js-title");
      // This is the actual question text. It changes when a new word is chosen.
      var caption = root.appendChild(document.createElement('p'));
      caption.setAttribute("class", "js-caption");
      // This is the actual "answer grid" where text can be entered.
      var table = root.appendChild(document.createElement('table'));
      table.setAttribute("class", "section-box");

      for (var y = 0; y < dict.tense_list.length + 1; ++y) {
        var row = table.appendChild(document.createElement('tr'));
        for (var x = 0; x < 4 + 1; ++x) {
          var td = row.appendChild(document.createElement(x && y ? "td" : "th"));
          if (y == 0) {
            td.appendChild(document.createTextNode(["*", "Plain", "Formal", "Negative Plain", " Negative Formal"][x]));
          } else if (x == 0) {
            td.appendChild(document.createTextNode(dict.tense_list[y-1]));
          } else {
            var textbox = td.appendChild(document.createElement("input"));
            textbox.dataset.formal = (x % 2) == 0;
            textbox.dataset.negative = x > 2;
          }
        }
      }

      // This is used to display extra information about the answers given.
      var infobox = root.appendChild(document.createElement('div'));
      infobox.setAttribute("class", "js-infobox");
    });

    // Now that the page is ready, generate some initial questions.
    reset();
  }

  /**
  @brief This function generates a new set of questions.
  */

  function reset() {
    // Reset all textboxes and hide the "score" section.
    var input = document.getElementsByTagName("input");
    for (var i = 0; i < input.length; ++i) {
      input[i].className = "";
      input[i].value = "";
    }
    document.getElementById("score").innerHTML = "";
    // Pick a new word for each section.
    QUESTION.forEach(function (dict, i) {
      var root = document.getElementById("question" + String(i));
      root.dataset.entry = Math.floor(Math.random() * dict.word_list.length);
      var entry = dict.word_list[root.dataset.entry];

      root.querySelector(".js-title").innerHTML = dict.about + ": " + entry.kanji;
      root.querySelector(".js-caption").innerHTML =
        ["Conjugate <b>", entry.kana, '</b> ("', entry.English, '") in the grid below.'].join('');
      root.querySelector(".js-infobox").innerHTML = "";
    });
  }

  /**
  @brief This function checks the given answers against the expected ones.
  */

  function check() {
    var score = 0;
    var total = 0;

    QUESTION.forEach(function (dict, i) {
      var root = document.getElementById("question" + String(i));
      var entry = dict.word_list[root.dataset.entry];
      var errors = [];

      dict.tense_list.forEach(function (tense, row) {
        var textboxes = root.querySelectorAll("tr:nth-child(" + String(row + 2) + ") input");
        Array.prototype.forEach.call(textboxes, function (elem) {
          var guess = elem.value.toLowerCase().toKana().toHiragana().replace(/\s/g, "");
          var formal = elem.dataset.formal == "true";
          var negative = elem.dataset.negative == "true";

          var options = { negative: negative, formal: formal };
          var kanji_answers = dict.conjugate(entry.kanji, tense, options);
          var kana_answers  = dict.conjugate(entry.kana, tense, options);

          if (kanji_answers.concat(kana_answers).indexOf(guess) >= 0) {
            elem.className = "right_answer";
            ++score;
          } else {
            var text = [];

            text.push("The <b>");
            text.push(formal ? "formal, " : "plain, ");
            text.push(negative ? "negative-" : "");
            text.push(tense);
            text.push("</b> form is ");

            kana_answers.forEach(function (kana, i) {
              var kanji = kanji_answers[i];
              if (i > 0) { text.push(" or "); }

              text.push([
                "<mark title='",
                (entry.prefer_kana ? kanji : kana),
                "'>",
                (entry.prefer_kana ? kana : kanji),
                "</mark>"
              ].join(''));
            });

            elem.className = "wrong_answer";
            errors.push(text.join(''));
          }
        });
      });

      var info_box = root.querySelector(".js-infobox");
      if (errors.length) {
        info_box.innerHTML = "<ul><li>" + errors.join("</li><li>") + "</li></ul>";
      } else {
        info_box.innerHTML = "<p>All answers are correct!</p>";
      }

      total += dict.tense_list.length * 4;
    });

    document.getElementById("score").innerHTML = "Score: " + String(score) + "/" + String(total);
  }

  // Initialise the page once it's ready.
  document.body.onload = setup;
})();
