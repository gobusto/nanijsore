/**
@file Code specific to the kanji.html page.
*/

(function () {
  "use strict";

  /**
  @brief Add a new "question" for a specific kanji.
  */

  function createKanjiQuestion(dict, kanji) {
    var test = document.getElementById("kanji-content");

    var question = test.appendChild(document.createElement("div"));
    question.setAttribute("class", "kanji-question");

    var big_kanji = question.appendChild(document.createElement("div"));
    big_kanji.setAttribute("class", "big-kanji");
    big_kanji.innerHTML = kanji;

    var readings = question.appendChild(document.createElement("div"));
    readings.setAttribute("class", "kanji-readings");

    for (var r in dict[kanji]) {
      var container = readings.appendChild(document.createElement("div"));
      container.setAttribute("class", "labelled-input");

      var label = container.appendChild(document.createElement("label"));
      label.innerHTML = r;

      var input = container.appendChild(document.createElement("input"));
      input.dataset.kanji = kanji;
      input.dataset.reading = r;

      var answers = readings.appendChild(document.createElement("div"));
      answers.setAttribute("class", "hidden-answer");
      answers.innerHTML = dict[kanji][r].join("<span class='split'></span>");
    }
  }

  /**
  @brief Re-initialise the test.
  */

  function startTestForGrade(grade) {
    // Highlight the selected grade button.
    var buttons = document.querySelectorAll(".js-grade-btn");
    Array.prototype.forEach.call(buttons, function (button) {
      var text = "btn js-grade-btn";
      if (button.dataset.grade === String(grade)) { text += " btn-success"; }
      button.setAttribute("class", text);
    });

    // Create test elements.
    var test = document.getElementById("kanji-content");
    test.innerHTML = "";

    var keys = Object.keys(KANJI_DICTIONARY[grade]);
    for (var i = 0; i < 10 && keys.length; ++i) {
      var kanji = keys.splice(Math.floor(Math.random() * keys.length), 1)[0];
      createKanjiQuestion(KANJI_DICTIONARY[grade], kanji);
    }

    // Create submit button.
    var container = test.appendChild(document.createElement("div"));
    container.setAttribute("class", "centred-box");
    container.id = "results-box";

    var btn = container.appendChild(document.createElement("a"));
    btn.href = "#top";
    btn.className = "btn btn-primary";
    btn.innerHTML = "Check Results";
    btn.onclick = function() { check(grade); };
  }

  /**
  @brief Called when the "Check Results" button is pressed.

  @todo Highlight WHICH of the acceptable answers was chosen.

  @todo Count the number of correct/incorrect answers + show it somewhere.
  */

  function check(grade) {
    var elem_set, elem_len, i;

    // Reveal all answers.
    elem_set = document.querySelectorAll(".hidden-answer");
    elem_len = elem_set.length;
    for (i = 0; i != elem_len; ++i)
      elem_set[i].setAttribute("class", "revealed-answer");

    // Update scores, etc.
    var scores = {};
    var totals = {};
    elem_set = document.getElementById("kanji-content").getElementsByTagName("input");
    elem_len = elem_set.length;
    for (i = 0; i != elem_len; ++i) {
      var kanji   = elem_set[i].dataset.kanji;
      var reading = elem_set[i].dataset.reading;
      var answer  = elem_set[i].value.toLowerCase().replace(/\s/g, '');

      if (reading == "on")
        answer = answer.toKana().toKatakana().replace('/', '・');
      else if (reading == "kun")
        answer = answer.toKana().toHiragana().replace('/', '・');

      if (!totals[reading])
        totals[reading] = scores[reading] = 0;
      ++totals[reading];

      if (KANJI_DICTIONARY[grade][kanji][reading].indexOf(answer) >= 0) {
        ++scores[reading];
        elem_set[i].setAttribute("class", "right_answer");
      } else {
        elem_set[i].setAttribute("class", "wrong_answer");
      }
    }

    var results = document.getElementById("results-box");
    results.innerHTML = "";
    for (var k in totals) {
      var elem = results.appendChild(document.createElement("div"));
      elem.className = "score";
      elem.innerHTML = k + ": " + String(scores[k]) + "/" + String(totals[k]);
    }
  }

  /**
  @brief Set up button events. Called after the page has loaded.
  */

  function setup() {
    var buttons = document.querySelectorAll(".js-grade-btn");
    Array.prototype.forEach.call(buttons, function (button) {
      button.onclick = function () { startTestForGrade(Number(this.dataset.grade)); };
    });
    startTestForGrade(1);
  }

  document.body.onload = setup;
})();
