/**
@file Code specific to the kana.html page.
*/

(function () {
  "use strict";

  /**
  @brief Replace the contents of all kana table cells with hiragana/katakana.

  This function also changes the visual styles of the hiragana/katakana buttons.

  @param katakana True = Switch to katakana, false = Switch to hiragana.
  */

  function kana_change(katakana) {
    var btns = document.querySelectorAll(".js-hiragana-btn, .js-katakana-btn");
    for (var b = 0; b < btns.length; ++b) {
      var c = btns[b].getAttribute("class").replace("btn-primary", "").trim();
      if (katakana != (c.indexOf("js-katakana-btn") < 0)) c += " btn-primary";
      btns[b].setAttribute("class", c);
    }

    var kana = document.querySelectorAll(".kana-grid td");
    for (var k = 0; k < kana.length; ++k) {
      if (katakana) kana[k].innerHTML = kana[k].innerHTML.toKatakana();
      else          kana[k].innerHTML = kana[k].innerHTML.toHiragana();
    }
  }

  /**
  @brief Initialise everything.
  */

  function setup() {
    var hiragana_buttons = document.querySelectorAll(".js-hiragana-btn");
    Array.prototype.forEach.call(hiragana_buttons, function (elem) {
      elem.onclick = function() { kana_change(false); };
    });

    var katakana_buttons = document.querySelectorAll(".js-katakana-btn");
    Array.prototype.forEach.call(katakana_buttons, function (elem) {
      elem.onclick = function() { kana_change(true); };
    });
  }

  document.body.onload = setup;
})();
