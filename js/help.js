/**
@file Code specific to the help.html page.
*/

(function () {
  "use strict";

  /**
  @brief Initialise everything.
  */

  function main() {
    var i = document.getElementById("convert_input");
    var o = document.getElementById("convert_output");

    document.getElementById("convert_toKataKana").onclick = function() {
      o.innerHTML = i.value.toKatakana();
    };
    document.getElementById("convert_toHiragana").onclick = function() {
      o.innerHTML = i.value.toHiragana();
    };
    document.getElementById("convert_toRomaji").onclick = function() {
      o.innerHTML = i.value.toRomaji();
    };
    document.getElementById("convert_toKana").onclick = function() {
      o.innerHTML = i.value.toKana();
    };
  }

  document.body.onload = main;
})();
