(function () {
  "use strict";

  var GRADES = ['1', '2', '3', '4', '5', '6', 'S'];

  function textForGrade(grade) {
    var dataset = KANJI_DICTIONARY[Number(grade) || 0];
    var items = Object.keys(dataset).length;
    return ['Grade ', grade, ' (', String(items), ' kanji)'].join('');
  }

  function createTableOfContents() {
    var page = document.getElementById('content-info-kanji');

    var ul = page.appendChild(document.createElement('ul'));
    GRADES.forEach(function (grade) {
      var li = ul.appendChild(document.createElement('li'));
      var a = li.appendChild(document.createElement('a'));
      a.href = '#grade-' + grade;
      a.innerHTML = textForGrade(grade);
    });
  }

  function main() {
    createTableOfContents();

    var page = document.getElementById('content-info-kanji');
    GRADES.forEach(function (grade) {
      // Create a section header:
      var title = page.appendChild(document.createElement('h2'));
      title.id = 'grade-' + grade;
      title.innerHTML = textForGrade(grade)
      // Go through each kanji for this grade:
      grade = Number(grade) || 0;
      Object.keys(KANJI_DICTIONARY[grade]).forEach(function (kanji) {
        var container = page.appendChild(document.createElement('div'));
        // Show the kanji itself:
        var what = container.appendChild(document.createElement('span'));
        what.innerHTML = kanji;
        // List any information about this kanji:
        Object.keys(KANJI_DICTIONARY[grade][kanji]).forEach(function (reading) {
          var line = container.appendChild(document.createElement('dl'));
          var label = line.appendChild(document.createElement('dt'));
          label.innerHTML = reading;
          var span = line.appendChild(document.createElement('dd'));
          span.innerHTML = KANJI_DICTIONARY[grade][kanji][reading].join(' / ');
        });
      });
    });
  }

  document.body.onload = main;
})();
