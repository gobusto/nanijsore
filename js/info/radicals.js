(function () {
  "use strict";

  function main() {

    var page = document.getElementById('content-info-kanji');

    // HACK: This is only here so that the CSS from the Kanji page works:
    page.appendChild(document.createElement('h2'));

    window.naniJS.RADICALS.forEach(function (dataset) {
      var container = page.appendChild(document.createElement('div'));
      // Show the kanji itself:
      var what = container.appendChild(document.createElement('span'));
      what.innerHTML = dataset.radical[0];
      // List any information about this kanji:
      Object.keys(dataset).forEach(function (property) {
        var line = container.appendChild(document.createElement('dl'));
        var label = line.appendChild(document.createElement('dt'));
        label.innerHTML = property;
        var span = line.appendChild(document.createElement('dd'));
        var value = typeof(dataset[property]) == 'object' ? dataset[property] : [dataset[property]];
        span.innerHTML = value.join(" / ");
      });
    });
  }

  document.body.onload = main;
})();
