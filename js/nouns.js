/**
@file Code specific to the nouns.html page.
*/

(function () {
  "use strict";

  // This defines the question set:
  var CATEGORIES = {
    "animal": ["bird", "cat", "cow", "dog", "elephant", "horse", "mouse", "octopus", "pig", "rabbit", "tiger"],
    "bodypart": ["ear", "eye", "finger", "foot", "hair", "hand", "mouth", "nose"],
    //"color": ["red", "orange", "yellow", "green", "teal", "blue", "purple", "pink", "brown", "black", "grey", "white"],
    "fruit": ["apple", "banana", "tangerine", "peach", "pear", "pineapple", "strawberry", "watermelon"],
    "weekdays": ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"],
    "writing": ["book", "comic", "letter", "magazine", "newspaper"]
  };

  // This function is used to insert a flag icon.
  function flag(lang) {
    var img = document.createElement("img");
    img.src = "image/" + lang + ".png";
    img.alt = lang;
    return img;
  }

  // This function translates the user interface into another language.
  function setUserInterfaceLanguage(lang) {
    document.getElementById("page_title").innerHTML = DICTIONARY[lang].title;
    document.getElementById("page_intro").innerHTML = DICTIONARY[lang].intro;
    document.getElementById("page_bttns").innerHTML = DICTIONARY[lang].buttons;

    Object.keys(CATEGORIES).forEach(function (category) {
      var words = DICTIONARY[lang].dictionary[category];
      document.getElementById(category).innerHTML = words ? words[0] : "???";
    });
  }

  // Create a language selection "menu" at the top of the page.
  function createLanguageSelectionFlags() {
    var navbar = document.getElementById("navbar");
    Object.keys(DICTIONARY).forEach(function (lang) {
      var img = navbar.appendChild(document.createElement("span")).appendChild(flag(lang));
      img.style.cursor = "pointer";
      img.onclick = function () { setUserInterfaceLanguage(lang); };
    });
  }

  // Create the "intro" section at the top of the page.
  function createIntroText() {
    var body = document.getElementById("noun-content");
    body.appendChild(document.createElement("h1")).id = "page_title";
    var intro = body.appendChild(document.createElement("div"));
    intro.id = "page_intro";
    intro.setAttribute('class', 'instructions');
  }

  // Create the questions for a specified category.
  function createQuestionCategory(category) {
    var body = document.getElementById("noun-content");

    var section = body.appendChild(document.createElement("div"));
    section.setAttribute("class", "noun-category");
    section.appendChild(document.createElement("h2")).id = category;

    CATEGORIES[category].forEach(function (noun) {
      var container = section.appendChild(document.createElement("div"));
      container.setAttribute("class", "noun-question");

      var img = container.appendChild(document.createElement("img"));
      img.alt = "???";
      img.src = "image/" + noun + ".png";

      var prefix = category + "_" + noun + "_";
      container.appendChild(document.createElement("input")).id = prefix + "answer";
      container.appendChild(document.createElement("div")).id = prefix + "info";
    });
  }

  // Create the "answer buttons" at the bottom of the page.
  function createAnswerButtons() {
    var body = document.getElementById("noun-content");
    var container = body.appendChild(document.createElement("div"));
    container.setAttribute("class", "noun-buttons");

    container.appendChild(document.createElement("span")).id = "page_bttns";

    Object.keys(DICTIONARY).forEach(function (lang) {
      var a = container.appendChild(document.createElement("a"));
      a.href = "#top";
      a.className = "btn";
      a.onclick = function () { checkAnswers(lang); };

      a.appendChild(flag(lang));
      a.appendChild(document.createTextNode(DICTIONARY[lang].name));
    });
  }

  // This is called when the user clicks on an "answer button" link.
  function checkAnswers(lang) {
    var score = 0;

    Object.keys(CATEGORIES).forEach(function (category) {
      CATEGORIES[category].forEach(function (noun) {
        var e = document.getElementById(category + "_" + noun + "_answer");

        var words = DICTIONARY[lang].dictionary[noun];
        if (words && words.indexOf(e.value.toLowerCase().replace(/\s/g, '')) >= 0) {
          ++score;
          e.className = "right_answer";
        } else {
          e.className = "wrong_answer";
        }

        var info = document.getElementById(category + "_" + noun + "_info");
        info.innerHTML = words ? words.join(", ") : "(translation missing)";
      });
    });

    // TODO: Output the score here.
  }

  // This function is called when the page first loads.
  function initialiseNounsPage() {
    createLanguageSelectionFlags();
    createIntroText();
    Object.keys(CATEGORIES).forEach(createQuestionCategory);
    createAnswerButtons();
    setUserInterfaceLanguage("en");
  }

  document.body.onload = initialiseNounsPage;
})();
