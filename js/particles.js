/**
@file
Contains all code specific to the particles page.
*/

/**
@module Particles
@description Encapsulates all code specific to the particles page.
*/

(function () {
  "use strict";

  // Pick a single random array index.
  function randomIndex(arr) { return Math.floor(Math.random() * arr.length); }

  // Pick a single random array item.
  function randomItem(arr) { return arr[randomIndex(arr)]; }

  // Pick `n` random keys from a given object.
  function randomKeys(obj, n) {
    var keys = Object.keys(obj);

    var results = [];
    for (var i = 0; i < n && keys.length; ++i)
      results.push(keys.splice(randomIndex(keys), 1)[0]); 
    return results;  
  }

  /**
  @function
  @memberof module:Particles

  @description Sets up a new list of questions when the page first loads (or if
  the reset button is clicked).
  */

  function reset() {
    var quiz_area = document.getElementById('particles-content');
    var questions = window.naniJS.SENTENCES;

    quiz_area.innerHTML = '';
    randomKeys(questions, 5).forEach(function (particle) {
      var question = randomItem(questions[particle]);
      var answers = [particle].concat(question.incorrect.particles);

      var container = quiz_area.appendChild(document.createElement('div'));
      container.className = 'section-box';

      var english = container.appendChild(document.createElement('div'));
      english.className = 'en';
      english.innerHTML = question.en;

      var japanese = container.appendChild(document.createElement('div'));
      japanese.className = 'jp';
      japanese.innerHTML = question.jp.replace('_', '<span class="gap"></span>');

      randomKeys(answers, answers.length).forEach(function (i) {
        var value = answers[i];

        var answer = container.appendChild(document.createElement('div'));
        answer.className = value == particle ? 'right' : 'wrong';

        var input = answer.appendChild(document.createElement('input'));
        input.id = [particle, value].join('_');
        input.name = particle;
        input.type = 'radio';
        input.value = value;

        var label = answer.appendChild(document.createElement('label'));
        label.setAttribute('for', input.id);
        label.innerHTML = value;
      });
    });
  }

  /**
  @function
  @memberof module:Particles

  @description Initial set-up stuff. Invoked when the page first loads.
  */

  function main() {
    document.getElementById('js-reset').onclick = reset;
    reset();
  }

  document.body.onload = main;
}());
