Licensing
=========

The flag icons are from the FamFamFam Flag Icons set, which can be found here:

<http://famfamfam.com/lab/icons/flags>

The days-of-the-week calendar icons were modified from the Tango calendar icon:

<http://tango.freedesktop.org>

All other images are from [Wikimedia Commons](https://commons.wikimedia.org) or
[Open Clip Art](https://openclipart.org). The exact license used for each image
varies; see the links below for further details.

Animals
-------

+ bird.png - <http://commons.wikimedia.org/wiki/File:Hirundinea_ferruginea_-Piraju,_Sao_Paulo,_Brasil-8.jpg>
+ cat.png - <http://commons.wikimedia.org/wiki/File:Gato_%282%29_REFON.jpg>
+ cow.png - <http://commons.wikimedia.org/wiki/File:Glanrind_1.jpg>
+ dog.png - <http://commons.wikimedia.org/wiki/File:Femelle_border_collie_de_l%27%C3%A9levage_de_Grimmaupr%C3%A8s.JPG>
+ elephant.png - <http://commons.wikimedia.org/wiki/File:Serengeti_Elefantenbulle.jpg>
+ horse.png - <http://commons.wikimedia.org/wiki/File:Wild_Horses_of_the_Outer_Banks.jpg>
+ mouse.png - <http://commons.wikimedia.org/wiki/File:Lab_mouse_mg_3216.jpg>
+ octopus.png - <http://commons.wikimedia.org/wiki/File:Pu_-_Octopus_vulgaris.jpg>
+ pig.png - <http://commons.wikimedia.org/wiki/File:Landsvin-gris.JPG>
+ rabbit.png - <http://commons.wikimedia.org/wiki/File:Coelho-5829.jpg>
+ tiger.png - <http://en.wiktionary.org/wiki/File:Panthera_tigris_tigris.jpg>

Bodyparts
---------

+ ear.png - <http://commons.wikimedia.org/wiki/File:Earrr.JPG>
+ eye.png - <http://commons.wikimedia.org/wiki/File:Folio_Makeup_by_Razel_Pong_02.jpg>
+ finger.png - <https://commons.wikimedia.org/wiki/File:3_fingers.JPG>
+ foot.png - <https://commons.wikimedia.org/wiki/File:Left_sole.jpg>
+ hair.png - <http://commons.wikimedia.org/wiki/File:Julian-Bashore-20141008_%282%29.jpg>
+ hand.png - <http://commons.wikimedia.org/wiki/File:Human-Hands-Front-Back.jpg>
+ mouth.png - <http://commons.wikimedia.org/wiki/File:Girl_after_eye_enlargement_procedure.jpg>
+ nose.png - <http://commons.wikimedia.org/wiki/File:Girl_before_eye_enlargement_procedure.jpg>

Fruit
-----

+ apple.png - <http://commons.wikimedia.org/wiki/File:Anna_Apple.jpg>
+ banana.png - <http://commons.wikimedia.org/wiki/File:Bananen_Frucht.jpg>
+ tangerine.png - <http://commons.wikimedia.org/wiki/File:Citrus_sinensis.jpg>
+ peach.png - <http://commons.wikimedia.org/wiki/File:Peach_%281%29.jpg>
+ pear.png - <http://commons.wikimedia.org/wiki/File:PearPhoto.jpg>
+ pineapple.png - <http://commons.wikimedia.org/wiki/File:Ananas~May_2008-1.jpg>
+ strawberry.png - <http://commons.wikimedia.org/wiki/File:FraiseFruitPhoto.jpg>
+ watermelon.png - <http://commons.wikimedia.org/wiki/File:Watermelon.svg>

Writing
-------

+ book.png - <https://openclipart.org/detail/78469/book>
+ comic.png - <http://commons.wikimedia.org/wiki/File:Wikipe-tan_manga_page4.jpg>
+ magazine.png - <http://commons.wikimedia.org/wiki/File:Ispectrum_issue_02.pdf>
+ newspaper.png - <https://openclipart.org/detail/23539/news-paper>

The "letter.png" image is a combination of two separate photographs:

+ <http://commons.wikimedia.org/wiki/File:Dopis_presidentu_Gottwaldovi_001a.jpg>
+ <http://commons.wikimedia.org/wiki/File:Enveloppe_blanche.png>
